﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;

namespace SocketHandler
{
    public enum CSocketMessageType
    {
        SocketStatusMessage,
        SocketErrorMessage,
        ChatterMessage
    }

    public class CSocketMessageEventArgs
    {
        public string message;
        public CSocketMessageType type;
    }

    public class CSocket
    {
        private StreamSocket chesssocket;

        private DataReader reader;

        private DataWriter writer;

        public string Hostname { get; set; }

        public string Port { get; set; }


        public CSocket()
        {
            this.BufferSize = 4096;
            this.chesssocket = new StreamSocket();
            this.Hostname = "127.0.0.1";
            this.Port = "5002";
        }

        public delegate void CSocketMessageEventHandler(object sender, CSocketMessageEventArgs e);

        public event CSocketMessageEventHandler ConnectedResultEvent;

        public event CSocketMessageEventHandler ReceivedDataEvent;

        public event CSocketMessageEventHandler DisconnectedEvent;

        public uint BufferSize { get; set; }

        public StreamSocket ChessSocket
        {
            get { return this.chesssocket; }
            set { this.chesssocket = value; }
        }

        public async void ConnectAsync()
        {
            this.chesssocket = new StreamSocket();

            try
            {
                HostName ficshost = new HostName(this.Hostname);
                await chesssocket.ConnectAsync(ficshost, this.Port);

                this.writer = new DataWriter(this.chesssocket.OutputStream);
                this.reader = new DataReader(this.chesssocket.InputStream);
                this.reader.InputStreamOptions = InputStreamOptions.Partial;
                //this.reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                await Task.Factory.StartNew(this.WaitForDataAsync);
                if (this.ConnectedResultEvent != null)
                {
                    CSocketMessageEventArgs args = new CSocketMessageEventArgs();
                    args.type = CSocketMessageType.SocketStatusMessage;
                    args.message = "Connected to remote host\n";
                    this.ConnectedResultEvent(this, args);
                }
            }
            catch (Exception except)
            {
                if (SocketError.GetStatus(except.HResult) == SocketErrorStatus.Unknown)
                {
                    throw;
                }
                if (this.ConnectedResultEvent != null)
                {
                    CSocketMessageEventArgs args = new CSocketMessageEventArgs();
                    args.type = CSocketMessageType.SocketErrorMessage;
                    args.message = "Connection to remote host failed with error: " + except.Message + "\n";
                    this.ConnectedResultEvent(this, args);
                }
            }
        }

        public void Disconnect()
        {
            if (this.chesssocket != null)
            {
                this.chesssocket.Dispose();
                this.chesssocket = new StreamSocket();
            }
        }

        public async void KnockOnDoorAsync()
        {
            if (this.chesssocket == null)
            {
                return;
            }

            this.writer.WriteString("\r");
            try
            {
                await this.writer.StoreAsync();
            }
            catch (Exception exception)
            {
                string m = exception.Message;
            }
        }

        public async void SendDataAsync(string s)
        {
            if (this.chesssocket == null)
            {
                return;
            }

            this.writer.WriteString(s);
            try
            {
                await this.writer.StoreAsync();
            }
            catch (Exception exception)
            {
                string m = exception.Message;
            }
        }

        private async void WaitForDataAsync()
        {
            if (this.chesssocket == null) return;

            try
            {
                while (true)
                {
                    UInt32 bytecount = await this.reader.LoadAsync(this.BufferSize);
                    byte[] content = new byte[reader.UnconsumedBufferLength];
                    reader.ReadBytes(content);
                    string text = Encoding.UTF8.GetString(content, 0, content.Length);
                    //string content = this.reader.ReadString(bytecount);
                    if (this.ReceivedDataEvent != null)
                    {
                        CSocketMessageEventArgs args = new CSocketMessageEventArgs();
                        args.type = CSocketMessageType.ChatterMessage;
                        args.message = text;
                        this.ReceivedDataEvent(this, args);
                    }
                }
            }
            catch (Exception exception)
            {
                if (this.DisconnectedEvent != null)
                {
                    CSocketMessageEventArgs args = new CSocketMessageEventArgs();
                    args.type = CSocketMessageType.ChatterMessage;
                    args.message = "Socket Error: " + exception.Message + "\n";
                    this.DisconnectedEvent(this, args);
                }
            }
        }
    }
}
